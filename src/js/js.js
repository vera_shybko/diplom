
function validate(sub_form,sub_email) {
var subValid = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
var subAddress = document.forms[sub_form].elements[sub_email].value;
if(subValid.test(subAddress) == false) {
    alert('Введите корректный e-mail');
    return false;
    }
}

function validate(feed_form,feed_email) {
	var feedValid = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var feedAddress = document.forms[feed_form].elements[feed_email].value;
	if(feedValid.test(feedAddress) == false) {
		alert('Введите корректный e-mail');
		return false;
		}
	}

window.onload = function() {
const anchors = document.getElementsByClassName('header-top__nav-button');
for(let anchor of anchors)
	{
		anchor.addEventListener('click', function (e)
		{
		  e.preventDefault();
			const anchorScr = anchor.getAttribute('href');
			document.querySelector(anchorScr).scrollIntoView({
				behavior: 'smooth',
				block: 'start'
			});					
		});
	}

const arrowAnchors = document.getElementsByClassName('header-bottom__circle-arrow');
for(let arrowAnchor of arrowAnchors)
	{
		arrowAnchor.addEventListener('click', function (e)
		{
			e.preventDefault();
			const arrowAnchorScr = arrowAnchor.getAttribute('href');
			document.querySelector(arrowAnchorScr).scrollIntoView({
				behavior: 'smooth',
				block: 'start'
			});					
		});
	}



const allImagesButt = document.getElementsByClassName('portfolio-input__button');
const allIMG = document.getElementById("portfolio-sort").getElementsByTagName("img");
const sortDiv = document.getElementById("portfolio-sort");
const regularDiv = document.getElementById("portfolio-image");
console.log (allIMG);

for(let butt of allImagesButt) {
	butt.addEventListener ("click", function(e) {
		e.preventDefault();
		var buttId = butt.getAttribute ("id");
		switch (buttId) {
			case "print":
				filtrImage ("print");
				break;
			case "identity":
				filtrImage ("identity");
				break;
			case "branding":
				filtrImage ("branding");
				break;
			case "web":
				filtrImage ("web");
				break;
			case "thml":
				filtrImage ("thml");
				break;
			case "wordpress":
				filtrImage ("wordpress");
				break;
			case "all":
				filtrImage ("all");
				break;
			default:
				break;
			}
		}
	);
}
function filtrImage (arrName) {
	if (arrName == "all") {
		regularDiv.style.display = "flex";
		sortDiv.style.display = "none";
		//for (let hiddenImage of allIMG) {
			//hiddenImage.style.display = "block";
			//}
		}
	else {
		regularDiv.style.display = "none";
		sortDiv.style.display = "block";
		for (let hiddenImage of allIMG) {
			if(hiddenImage.getAttribute("class") == arrName) {
				hiddenImage.style.display = "inline";
			}
			else {
				hiddenImage.style.display = "none";
			}
			}
		}
	}
}



