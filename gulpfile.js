const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const fonts = require('gulp-font');
const script = require('gulp-script');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const browserSync = require('browser-sync');

gulp.task('image', function () {
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(gulp.dest('./build/assets/img'));
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 16 versions'))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('script', function () {
    gulp.src('./src/**/*.js')
        .pipe(gulp.dest('./build'));
});

gulp.task('fonts', function () {
    gulp.src('./src/**/*.ttf')
        .pipe(gulp.dest('./build'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        },
    })
});

gulp.task('watch', ['sass', 'html', 'image', 'fonts', 'script','browserSync'], function () {
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('./src/img/*', ['image']);
    gulp.watch('./src/fonts/*', ['fonts']);
    gulp.watch('./src/**/*.js', ['script']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'fonts', 'image', 'script']);